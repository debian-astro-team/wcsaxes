Source: wcsaxes
Section: python
Priority: optional
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Build-Depends: debhelper (>= 9),
               dh-python,
               python-all,
               python-astropy (>= 1.0),
               python-astropy-helpers,
	       python-matplotlib,
	       python-pytest,
               python-setuptools,
	       python-tk,
               python3-all,
               python3-astropy,
               python3-astropy-helpers,
	       python3-matplotlib,
	       python3-pytest,
               python3-setuptools,
	       python3-tk,
	       xauth,
	       xvfb
Standards-Version: 3.9.8
Homepage: https://github.com/astrofrog/wcsaxes
Vcs-Git: https://anonscm.debian.org/git/debian-astro/packages/wcsaxes.git
Vcs-Browser: https://anonscm.debian.org/cgit/debian-astro/packages/wcsaxes.git
X-Python-Version: >= 2.7
X-Python3-Version: >= 3.3

Package: python-wcsaxes
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
Description: Framework for plotting astronomical and geospatial data (Python 2)
 WCSAxes is a framework for making plots of Astronomical data in
 Matplotlib. It is affiliated with the Astropy project and is intended for
 inclusion in the Astropy package once stable.
 .
 This package contains the Python 2 version of the package.

Package: python3-wcsaxes
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-matplotlib
Description: Framework for plotting astronomical and geospatial data (Python 3)
 WCSAxes is a framework for making plots of Astronomical data in
 Matplotlib. It is affiliated with the Astropy project and is intended for
 inclusion in the Astropy package once stable.
 .
 This package contains the Python 3 version of the package.
